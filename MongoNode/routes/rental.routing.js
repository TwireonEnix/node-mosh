const rentalController = require('../controllers/rental.controller');
const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/check-auth');

router.get('/', rentalController.getRentals);
router.post('/create', checkAuth, rentalController.createRental);

module.exports = router;