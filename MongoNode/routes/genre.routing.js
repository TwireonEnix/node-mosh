const express = require('express');
const router = express.Router();
const genreController = require('../controllers/genre.controller');
const checkAuth = require('../middleware/check-auth');

router.get('/', genreController.getAll);
router.get('/:id', genreController.getOne);
router.post('/create', checkAuth, genreController.createGenre);
router.put('/update/:id', checkAuth, genreController.modifyGenre);
router.delete('/delete/:id', checkAuth, genreController.deleteGenre);

module.exports = router;
