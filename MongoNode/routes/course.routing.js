const express = require('express');
const router = express.Router();
const courseController = require('../controllers/course.controller');

router.get('/', courseController.test);
router.get('/courses', courseController.getCourses);
router.get('/ex1', courseController.moshExersice1);
router.get('/ex2', courseController.moshExersice2);
router.get('/ex3', courseController.moshExersice3);
router.get('/queryFirst/:id', courseController.updateQueryFirst);
router.get('/updateFirst/:id', courseController.updateFirst);
router.delete('/delete/:id', courseController.removeCourse);

module.exports = router;
