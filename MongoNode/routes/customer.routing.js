const express = require('express');
const router = express.Router();
const customerController = require('../controllers/customer.controller');
const checkAuth = require('../middleware/check-auth');

router.get('/', customerController.getAll);
router.get('/:id', customerController.getOne);
router.post('/create', checkAuth, customerController.createCustomer);
router.put('/update/:id', checkAuth, customerController.modifyCustomer);
router.delete('/delete/:id', checkAuth, customerController.deleteCustomer);

module.exports = router;