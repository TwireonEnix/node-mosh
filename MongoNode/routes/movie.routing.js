const express = require('express');
const router = express.Router();
const movieController = require('../controllers/movie.controller');
const checkAuth = require('../middleware/check-auth');
const checkAdmin = require('../middleware/check-admin');

router.get('/', movieController.getAll);
router.get('/id', movieController.getOne);
/* arreglo de middlewares */
router.post('/create', [checkAuth, checkAdmin], movieController.createMovie);
router.put('/update/:id', [checkAuth, checkAdmin], movieController.updateMovie);
router.delete('/delete/:id', [checkAuth, checkAdmin], movieController.deleteMovie);

module.exports = router; 