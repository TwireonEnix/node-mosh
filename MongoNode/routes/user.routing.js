const userController = require('../controllers/user.controller');
const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/check-auth');

router.get('/me', checkAuth, userController.getUser);
router.post('/create', userController.createUser);
router.post('/login', userController.login);

module.exports = router;