const mongoose = require('mongoose');
const app = require('./app');
const http = require('http');

mongoose.connect('mongodb+srv://' +
process.env.BLOG_DB_USER + ':' +
process.env.BLOG_DB_PASS + '@' +
process.env.BLOG_DB_HOST + '/blogDB?retryWrites=true01', { useNewUrlParser: true})
  .then(() => {
    console.log('Connected to db');
    const port = process.env.PORT || 3000;
    app.set('port', port);
    const server = http.createServer(app);
    server.listen(port, () => {
      console.log('Running successfully at ' + port);
    });
  })
  .catch(err => {
    console.log('Connection failed!' + err.message);
  });
