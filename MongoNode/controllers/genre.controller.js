const Genre = require('../models/genre.model');
const joi = require('joi');

const joiSchema = {
  name: joi
    .string()
    .min(4)
    .required()
};

const genreController = {
  getAll: (req, res) => {
    throw new Error('Could not get the genres');
    Genre.find()
      .sort('name')
      .then(genres => res.status(200).json(genres));
  },

  getOne: (req, res) => {
    Genre.find({
      _id: req.params.id
    })
      .then(genre => {
        if (!genre)
          return res.status(404).json({
            message: 'Genre not found'
          });
        res.status(200).json(genre);
      })
      .catch(err => {
        res.status(500).json({ error: err });
      });
  },

  createGenre: (req, res) => {
    const genre = new Genre({
      name: req.body.name
    });
    genre
      .save()
      .then(newGenre => {
        res.status(200).json({
          message: 'created',
          genre: newGenre
        });
      })
      .catch(err =>
        res.status(500).json({
          message: 'something happened..',
          error: err.message
        })
      );
  },

  modifyGenre: (req, res) => {
    const { error } = joi.validate(req.body, joiSchema);
    if (error) return res.status(400).send(error.details[0].message);
    Genre.findByIdAndUpdate(
      req.params.id,
      {
        name: req.body.name
      },
      {
        new: true
      }
    )
      .then(genre => {
        if (!genre)
          return res.status(404).json({
            message: 'Genre not found'
          });
        res.status(200).json({
          message: 'Success updating',
          updatedGenre: genre
        });
      })
      .catch(err =>
        res.status(500).json({
          error: err.message
        })
      );
  },

  deleteGenre: (req, res) => {
    Genre.findByIdAndRemove(req.params.id)
      .then(deletedGenre => {
        if (!deletedGenre)
          return res.status(404).json({
            message: 'Genre not found'
          });
        res.status(200).json({
          message: 'Success deleting',
          deletedGenre: deletedGenre
        });
      })
      .catch(err =>
        res.status(500).json({
          error: err.message
        })
      );
  }
};

module.exports = genreController;
