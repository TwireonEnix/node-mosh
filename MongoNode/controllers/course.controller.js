const Course = require('../models/course.model');
const courseController = {

  test: (req, res, next) => {
    const course = new Course({
     // name: 'Angular Course',
      author: 'Bubu2',
      category: '-',
      tags: [],
      isPublished: true,
      price: 20
    });
    course.save().then(doc => {
      res.status(200).json({
        document: doc
      });
    }).catch(err => {
      /* Análisis de cada error */
      let errors = []; 
      for(field in err.errors) {errors.push(err.errors[field].message);}
      res.status(500).json({
        message: 'Couldn\'t add course',
        error: errors
      });
    });
  },

  getCourses: (req, res, next) => {
    /* OPERADORES DE COMPARACIÓN
    - eq (equal)
    - ne (not equal)
    - gt (greater than)
    - gte (greater than or equal to)
    - lt (less than)
    - lte (less than or equal to)
    - in
    - nin (not in) 

    Entonces para hacer queries con los comparadores en lugar de que los pares keyvalue sean estáticos
    en la sentencia, el valor se cambia por otro kvp (key value pair) donde se pueden utilizar los 
    comparadores e.g.:

    Thing.find({ price: {$gte: 10} })

    Por lo tanto esta sentencia devolverá los elementos cuyo price sea mayor o igual a 10. Los operadores se pueden
    seguir agregando en el objeto para más comparaciones e.g.:

    Thing.find({ price: {$gte: 10, $lte: 20} })

    El query será entonces los elementos cuyo precio esté entre 10 y 20.
    Los operadores tambien aceptan arreglos como parámetros de comparación:

    Thing.find({ price: { $in: [10, 15, 20]} })

    El query devolverá entonces los elementos cuyo precio solo sea 10, 15 y 20.

    OPERADORES LÓGICOS

    - and
    - or

    Para hacer esto el find se pasa sin parámetros y se concatena el operador lógico con los filtros
    en un arreglo de objetos. La sintaxis de ambos operadores es la misma. e.g.:

    Thing.find().or([{author: 'Mosh'}, {isPublished: true}])

    EXPRESIONES REGULARES

    https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions
    En el caso anterior únicamente se devolverán los elementos que sean exactamente iguales a los filtros.
    por lo tanto, autores como Moshfegh, o Mosh Hamedani no apareceran en los resultados. Para esto llegan
    las expresiones regulares.
    La sintaxis de las expresiones regulares son: 
    
    .find({ author: /pattern/ })

    Inicia con: /^Mosh/
    Termina con: /Hamedani$/i (el signo de dólar en regex significa el fin de una cadena )

    Las regex son case sensitive, para quitar la restricción se le agrega una i al final. Como en ejemplo anterior

    Contenga la palabra: /.*Mosh.* / (.* significa que cualquier cosa puede haber ahí)

    COUNTS: 

    En lugar de seleccionar elementos a regresar con select, se regresan el número de elementos que regrese el query
    que cumplan cualquier filtro impuesto previamente

    .find({ author: 'Mosh'}).count()

    SKIPS: 
    Los skip se utilizan para implementar paginación
    Se definen constantes que, en una aplicación real, son partes del query.

    /api/courses?pageNumber=2&pageSize=10

    const pageNumber = 2;
    const pageSize = 10;

    Entonces la fórmula para hacer el skip, después de los filtros del query es
    .skip((pageNumber - 1) * pageSize)
    .limit(pageSize)

    Con esto se pueden regresar los documentos de una página 


    */
    /* los finds regresan un objeto tipo documento que es una promesa igual find se puede filtrar.
    Los filtros se pasan como un objeto js e implicitamente se manejan con un and. */
    // Course.find({tags: 'node'})
    Course.find()

    /* Los queries se pueden ser delimitados con funciones propias de mongoose.
    - limit: limita el número de registros que se obtienen
    - sort: se le da un ordenamiendo en alguna propiedad, el 1 es en ascendente -1 descendente
    - select: se selecciona sólo las propiedades que se quieren devolver. (_id es regresado por default) */ 
      .limit(10).sort({ name: 1}) // .select({name: 1, tags: 1})
      .then(rows => res.status(200).json({docs: rows}))
      .catch(err => res.status(500).json({error: err}));
  },

  moshExersice1: (req, res, next) => {
    Course.find({isPublished: true, tags: 'backend'}).sort({name: 1}).select({name: 1, author: 1})
      .then(rows => res.status(200).json(rows))
      .catch(err => res.status(500).json({message: 'error', error: err}));
  },

  moshExersice2: (req, res, next) => {

    /* Ambas maneras de hacer el query con un or o con un operador de mongo $in:
    Igualmente ambas maneras de utilizar el sort y select */

    // Course.find({isPublished: true, tags: { $in: ['backend', 'frontend'] }}).sort('-price').select('name author price')
    Course.find({isPublished: true}).or([{tags: 'backend'}, {tags: 'frontend'}]).sort('-price').select('name author price')
      .then(rows => res.status(200).json(rows))
      .catch(err => res.status(500).json({message: 'error', error: err}));
  },

  moshExersice3: (req, res, next) => {
  Course.find({isPublished: true}).or([ {price: {$gte: 15}}, {name: /.*by.*/i } ])
    .then(rows => res.status(200).json(rows))
    .catch(err => res.status(500).json({message: 'error', error: err}));
  },

  /* update query first sirve para validar algun tipo de restricción sobre el documento antes de realizar el cambio*/
  updateQueryFirst: (req, res, next) => {
    Course.findById(req.params.id).then(course => {
      if (course) return course; 
      else res.status(400).json({message: 'Course not found'});
    }).then(result => {
      const updatedCourse = result;
      /* se llama al método set para modificar el objeto obtenido y se modificará en base al body de la petición */
      updatedCourse.set({
        isPublished: true,
        author: 'Another Author'
      });
      console.log(updatedCourse.bsonsize());
      updatedCourse.save().then(updated => res.status(200).json({message: 'updated', doc: updated}));
    })
    .catch(error => res.status(500).json({message: 'Fetching post failed!'}));
  },

  updateFirst: (req, res, next) => {
    /* el segundo parámetro de findOneAndUpdate toma los operadores de update de mongo
    $inc, $max, $set, etc. la opción de new: true, devuelve el objeto actual, en lugar del anterior.*/
    Course.findOneAndUpdate(req.params.id, {
      $set: {
        author: 'Bubu',
        isPublished: false
      }
    }, {new: true}).then(result => {
      res.status(200).json({resultado: result});
    }).catch(error => res.status(500).json({message: 'Fetching post failed!'}));
  },

  removeCourse: (req, res, next) => {
    /* El método deleteOne recibe como parámetros un filtro o un objeto query */
    Course.deleteOne({ _id: req.params.id}).then(result => {
      if (result.n > 0 ) res.status(200).json({message: 'deleted', result: result});
      else res.status(404).json({message: 'course not found'})
    }).catch(err => res.status(500).json({message: 'error', error: err}));
  }



};

module.exports = courseController;