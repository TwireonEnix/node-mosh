const { Rental, validate } = require('../models/rental.model');
const { Customer } = require('../models/customer.model');
const { Movie } = require('../models/movie.model');
const Fawn = require('fawn');
const mongoose = require('mongoose');

Fawn.init(mongoose);

const rentalController = {

  getRentals: (req, res) => {
    /* populate para multiples (cómo se hace un nested? Respuesta en código) */
    Rental.find()
      /* Poner atención en especial en éstas dos líneas: cuando hay 2 documentos embebidos se encadenan los
      populate, pero al segundo se le dan parámetros extra, los primeros el primer path es es la primera 
      referencia, el segundo parámetro es otro objeto de populate, ya del documento embebido dentro de la 
      primera referencia, igualmente con su respectivo select */
      .populate('customerId', 'name')
      .populate({ path: 'movieId', populate: {path: 'genreId', select: 'name'} })
      .then(result => {res.status(200).json(result[0])})
      .catch(err => res.status(500).json({error: err.message})
    );
  },

  createRental: (req, res) => {
    /* validar petición con el modelo*/
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    /* verificar que el usuario existe */
    Customer.findById(req.body.customerId).select('_id').then(customer => {
      if (!customer) return res.status(400).json({message: 'Invalid customer'});
      return customer;
    }).then(customer => {
      /* verificar que la película y que está en stock */
      Movie.findById(req.body.movieId).select('_id numberInStock').then(movie => {
        if (!movie) return res.status(400).json({message: 'Invalid movie'});
        if (movie.numberInStock === 0) return res.status(400).json({message: 'Movie not available'});
        return { movie: movie, customerId: customer._id };
      }).then(customerAndMovie => {
        /* Método no recomendado porque no usa transacciones
        Movie.findByIdAndUpdate(customerAndMovie.movie,{
          $inc: {
            numberInStock: 1
          }
        }, {new: true}).then(result => {
          console.log(result);
          if (result) return res.status(200).json(result);
          else res.status(400).json({message: 'update failed'});
        }) 

        El paquete fawn simula transacciones de bds relacionales en mongoose usando tareas que devuelven promesas
        y hacen rollbacks en caso de que alguna falle
        */
        const rental = new Rental({
          customerId: req.body.customerId,
          movieId: req.body.movieId
        });
        const task = new Fawn.Task();
        task.save('rentals', rental)
        /* por qué no habrá funcionado el req.body.movieId? */
          .update('movies', { _id: customerAndMovie.movie._id }, {
            $inc: { numberInStock: -1 }
          }).run().then(result => {
            res.status(200).json({message: 'Success in transaction', results: rental})
          })
      })
    }).catch(err => res.status(500).json({error: err.message}));
  }

}

module.exports = rentalController;