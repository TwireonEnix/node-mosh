const { User, validateUser, validateLogin } = require('../models/user.model');
/* convención para guardar lodash, este es un paquete para modificar objetos js */
const _ = require('lodash');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
// const config = require('config');

const userController = {

  getUser: (req, res, next) => {
    User.findById(req.user._id).select('-password').then(user => res.status(200).json(user)).catch(err => res.status(500).json(err.message));
  },

  createUser: (req, res) => {
    const { error } = validateUser(req.body);
    if (error) return res.status(400).json(error.details[0].message);
    let createdUser;
    bcrypt.genSalt(10).then(salt => {
      return bcrypt.hash(req.body.password, salt);
    }).then(hash => {
      const user = new User({
        name: req.body.name,
        email: req.body.email,
        password: hash
      });
      return user.save();
    }).then(result => {
      createdUser = result;
      return jwt.sign({_id: createdUser._id, isAdmin: createdUser.isAdmin}, 'jwtPrivateKey');
    }).then(token => {
      /* genera automáticamente el token y lo agrega a la cabecera de la respuesta*/
      res.header('x-auth-token', token).status(201).json({message: 'Registered', user: createdUser});
    }).catch(error => next(error));
  },

  login: (req, res, next) => {
    const { error } = validateLogin(req.body);
    if (error) return res.status(404).json({message: 'Invalid email or password'});
    /* si hay más promesas dentro de promesas, regresas la promesa, así se encadenan (creo)
    las promesas. Fijarse que las variables y constantes declaradas dentro del scope de la promesa perteneces sólo a esta. 
    Entonces para guardar el usuario obtenido, hay que declarar una variable (no constante) antes del bloque asíncrono
    para poder utilizarlo en la generación del token. */
    let fetchedUser; 
    User.findOne({ email: req.body.email }).then(user => {
      if (!user) return res.status(404).json({message: 'Invalid email or password'});
      fetchedUser = user;
      return bcrypt.compare(req.body.password, user.password);
    }).then(isValid => {
      if (!isValid) return res.status(404).json({message: 'Invalid email or password'});
      /* no guardar nunca en el código la llave del jwt, utilizar variables de ambiente con config (a pesar de que 
      meto env variables con export, node nunca las detecta.) */
      // console.log(config.get('jwtPrivateKey'));
      return jwt.sign({_id: fetchedUser._id, isAdmin: fetchedUser.isAdmin}, 'jwtPrivateKey');
    }).then(token => {
      res.status(200).json({message: 'Successful login', token: token});
    }).catch(error => next(error));
  }
};

module.exports = userController;
