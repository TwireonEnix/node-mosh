/* Object de-structuring: deestructuración de un objeto para no tener Customer.validate
solamente se llama validate. */
const { Customer, validate } = require('../models/customer.model');

const customerController = {
  getAll: (req, res) => {
    Customer.find()
      .sort('name')
      .then(customers => res.status(200).json(customers));
  },

  getOne: (req, res) => {
    Customer.find({
      _id: req.params.id
    })
      .then(customer => {
        if (!customer)
          return res.status(404).json({
            message: 'Customer not found'
          });
        res.status(200).json(customer);
      })
      .catch(err => {
        res.status(500).json({ error: err.message });
      });
  },

  createCustomer: (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    const customer = new Customer({
      name: req.body.name,
      isGold: req.body.isGold,
      phone: req.body.phone
    });
    customer
      .save()
      .then(newCustomer => {
        res.status(200).json({
          message: 'created',
          customer: newCustomer
        });
      })
      .catch(err =>
        res.status(500).json({
          message: 'something happened..',
          error: err.message
        })
      );
  },

  modifyCustomer: (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    Customer.findByIdAndUpdate(
      req.params.id,
      {
        name: req.body.name,
        isGold: req.body.isGold,
        phone: req.body.phone
      },
      {
        new: true
      }
    )
      .then(updatedCustomer => {
        if (!updatedCustomer)
          return res.status(404).json({
            message: 'Customer not found'
          });
        res.status(200).json({
          message: 'Success updating',
          updatedCustomer: updatedCustomer
        });
      })
      .catch(err =>
        res.status(500).json({
          error: err.message
        })
      );
  },

  deleteCustomer: (req, res) => {
    Customer.findByIdAndRemove(req.params.id)
      .then(deletedCustomer => {
        if (!deletedCustomer)
          return res.status(404).json({
            message: 'Customer not found'
          });
        res.status(200).json({
          message: 'Success deleting',
          deletedCustomer: deletedCustomer
        });
      })
      .catch(err =>
        res.status(500).json({
          error: err.message
        })
      );
  }
};

module.exports = customerController;
