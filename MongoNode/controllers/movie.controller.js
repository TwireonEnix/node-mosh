const { Movie, validate } = require('../models/movie.model');
const { Genre } = require('../models/genre.model');

const movieController = {
  getAll: (req, res) => {
    Movie.find().sort('title').then(movies => res.status(200).json(movies))
      .catch(err => res.status(500).json({error: err.message}));
  },

  getOne: (req, res) => {
    Movie.find({_id: req.params.id}).then(movie => {
      if (!movie) return res.status(404).json({message: 'Movie not found'});
      res.status(200).json(movie);
    }).catch(err => res.status(500).json({error: err.message}));
  },
  
  createMovie: (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    const movie = new Movie({
      title: req.body.title,
      genreId: req.body.genreId,
      numberInStock: req.body.numberInStock,
      dailyRentalRate: req.body.dailyRentalRate
    });
    movie.save().then(newMovie => {
      res.status(200).json({message: 'created', movie: newMovie});
    }).catch(err => res.status(500).json({error: err.message}));
  },

  updateMovie: (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    Movie.findByIdAndUpdate(req.params.id, {
      title: req.body.title,
      genreId: req.body.genreId,
      numberInStock: req.body.numberInStock,
      dailyRentalRate: req.body.dailyRentalRate
    },  {new: true}).then(updatedMovie => {
      if (!updatedMovie) return res.status(404).json({message: 'Movie not found'});
      res.status(200).json({message: 'Success updating', updatedMovie: updatedMovie});
    }).catch(err => res.status(500).json({error: err.message}));
  },

  deleteMovie: (req, res) => {
    Movie.findByIdAndRemove(req.params.id).then(deletedMovie => {
      if (!deletedMovie) return res.status(404).json({message: 'Movie not found'});
      res.status(200).json({message: 'Deleted', movie: deletedMovie});
    }).catch(err => res.status(500).json({error: err.message}));
  },

}

module.exports = movieController;