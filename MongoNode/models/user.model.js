const mongoose = require('mongoose');
const Joi = require('joi');
/* para que se valide que el correo y el nombre serán unicos */
const uniqueValidator = require('mongoose-unique-validator');
/* joi password complexity agrega requerimientos a las contraseñas*/
// const PasswordComplexity = require('joi-password-complexity');

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    minlength: 5,
    maxlength: 50,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true,
    minlength: 5,
    maxlength: 50,
  },
  password: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 100,
  },
  isAdmin: Boolean
});

userSchema.plugin(uniqueValidator);

function validateUser(user) {
  const schema = {
    name: Joi.string().required().min(5).max(50),
    email: Joi.string().required().email(),
    password: Joi.string().required()
  };

  return Joi.validate(user, schema);
}

function validateLogin(credentials) {
  const schema = {
    email: Joi.string().required().email(),
    password: Joi.string().required()
  }
  return Joi.validate(credentials, schema);
}

module.exports.User = mongoose.model('User', userSchema);
module.exports.validateUser = validateUser;
module.exports.validateLogin = validateLogin;