const mongoose = require('mongoose');
const joi = require('joi');
/* paquete npm para verificar la validez de ObjectId en joi */
joi.objectId = require('joi-objectid')(joi);
const rentalSchema = new mongoose.Schema({
  customerId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Customer',
    required: true
  },
  movieId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Movie',
    required: true
  },
  dateOut: {
    type: Date,
    required: true,
    default: Date.now
  },
  dateReturned: {
    type: Date
  },
  rentalFee: {
    type: Number,
    min: 0
  }
});

function validateRental(rental) {
  const joiSchema = {
    /* validez de objectid adicional al paquete base de joi */
    customerId: joi.objectId().required(),
    movieId: joi.objectId().required()
    /* No se incluyen las demás porque esas deben ser calculadas por el backend*/
  };
  return joi.validate(rental, joiSchema);
}

module.exports.Rental = mongoose.model('Rental', rentalSchema);
module.exports.validate = validateRental;