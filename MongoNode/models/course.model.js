const mongoose = require('mongoose');

const courseSchema = new mongoose.Schema({
  /* Por default todas las propiedades aquí son opcionales, pero se pueden especificar validadores */
  name: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 255
  },
  category: {
    type: String,
    /* enum sirve para que ocupe alguno de los valores que se especifican en el arreglo, de lo contrario lanza un error  */
    enum: ['web', 'mobile'],
    required: true
  },
  author: String,

  /*
  tags: {
    type: Array,
    // Para hacer un validador costumizado. 
    validate: {
      validator: function(v) {
        // Si no se pasa  un arreglo se regresaría que no se puede leer el valor length de null, entonces se valida que exista algo en v 
        return v && v.length > 0;
      },
      message: 'A course should has at least one tag'
    }
  },
  */

  /* Validación asíncrona. Si no se puede validar de manera inmediata una propiedad porque dependa de algún servicio externo, se crea
  una validación asíncrona con la bandera isAsync dentro del validador en true, y haciendo un callback en la función */
  tags: {
    type: Array,
    validate: {
      /* Importante para que funcionen las validaciones asíncronas. */
      isAsync: true,
      validator: function(v, callback) {
        setTimeout(() => {
          // do some async work. Aquí es donde se mandarían a llamar los recursos externos y regresarlos una vez que se tengan.
          const result = v && v.length > 0;
          callback(result);
        }, 2000);
      },
      message: 'A course should has at least one tag'
    }
  },

  date: {
    type: Date,
    default: Date.now
  },
  isPublished: Boolean,
  price: {
    type: Number,
    /* no se puede utilizar una función flecha porque el this es únicamente en el scope de la función de flecha */
    required: function() {
      return this.isPublished;
    },
    min: 10,
    max: 20
  }
});

/* los tipos de variables que se pueden seleccionar son: 
-string
-number
-date
-buffer (para datos binarios)
- boolean
-objectID para asignar identificadores únicos
-arreglos */

module.exports = mongoose.model('Course', courseSchema);
