const mongoose = require('mongoose');
const joi = require('joi');
joi.objectId = require('joi-objectid')(joi);

const movieSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    minlength: 2,
    maxlength: 255
  },
  genreId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Genre',
    required: true,
  },
  numberInStock: {
    type: Number,
    required: true,
    min: 0,
    max: 255
  },
  dailyRentalRate: {
    type: Number,
    required: true,
    min: 0,
    max: 255
  }
});

function validateMovie(movie) {
  const joiSchema = {
    title: joi.string().required().min(2).max(255),
    genreId: joi.objectId().required(),
    numberInStock: joi.number().required().min(0),
    dailyRentalRate: joi.number().required().min(0)
  };
  return joi.validate(movie, joiSchema);
}

module.exports.Movie = mongoose.model('Movie', movieSchema);
module.exports.validate = validateMovie;