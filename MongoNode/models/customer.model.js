const mongoose = require('mongoose');
const joi = require('joi');

const customerSchema = new mongoose.Schema({
  isGold: {
    type: Boolean,
    required: true
  },
  name: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 20
  },
  phone: {
    type: String,
    required: true
  }
});

function validateCustomer(customer) {
  const joiSchema = {
    isGold: joi.boolean().required(),
    name: joi.string().min(5).max(20).required(),
    phone: joi.string().required()
  };
  return joi.validate(customer, joiSchema);
}

/* Al exportar ambas propiedades se pueden desestructurar cuando se importan en otro modulo */

module.exports.Customer = mongoose.model('Customer', customerSchema);
module.exports.validate = validateCustomer;