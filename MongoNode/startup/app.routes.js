const courseRoutes = require('../routes/course.routing');
const genreRoutes = require('../routes/genre.routing');
const customerRoutes = require('../routes/customer.routing');
const movieRoutes = require('../routes/movie.routing');
const rentalRoutes = require('../routes/rental.routing');
const userRoutes = require('../routes/user.routing');
const helmet = require('helmet');
const morgan = require('morgan');
const express = require('express');
const errorMiddleware = require('../middleware/error');

module.exports = (app) => {
  app.use(express.json());
  app.use(express.urlencoded({
    extended: true
  }));
  app.use(helmet());
  app.use(morgan('dev'));

  app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Request-With, Content-Type, Accept, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH, DELETE, PUT, OPTIONS');
    next();
  });

  app.use('/api/course', courseRoutes);
  app.use('/api/genre', genreRoutes);
  app.use('/api/customer', customerRoutes);
  app.use('/api/movie', movieRoutes);
  app.use('/api/rental', rentalRoutes);
  app.use('/api/user', userRoutes);

  /* Sentencia para manejar los errores internos del server de manera global con un middleware al atrapar excepciones en código asíncrono*/
  app.use(errorMiddleware);
}