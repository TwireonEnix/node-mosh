const winston = require('winston');

module.exports = (error, req, res, next) => {
  /* para ocupar el logeo por winston el primer parámetro es errores por niveles:
  -error
  -warn
  -info
  -verbose
  -debug
  -silly */
  winston.error(error.message, error);
  res.status(500).json({message: 'Something failed', error: error});
};