const jwt = require('jsonwebtoken');
config = require('config')

module.exports = (req, res, next) => {
  const token = req.header('x-auth-token');
  if (!token) res.status(401).json({nessage: 'Access denied'});
  try {
    const decodedToken = jwt.verify(token, 'jwtPrivateKey');
    req.user = decodedToken;
    next();
  } catch (error) {
    res.status(400).json({message: 'Invalid Token'});
  }
}