require('express-async-errors');
const express = require('express');
const app = express();
require('./startup/app.routes')(app);

module.exports = app;


// const config = require('config');
/* default logger -*/

/*console.log(process.env);
console.log(process.env.moshCoursejwtPrivateKey);
console.log(config.get('hola'));
if (!config.get('hola')) {
  console.error('Fatal Error: jwtPrivateKey is not defined.');
  process.exit(1);
}
*/

// throw new Error();

// winston.add(new winston.transports.File({ filename: "logfile.log" }));

/* manejo de excepción fuera del contexto de express
process.on('uncaughtException', ex => {
  winston.error(ex.message, ex);
  process.exit(1);
}) */

//winston.hande(new winston.transports.File({filename: 'UnhandleExceptions.log'}));

/* process.on('unhandledRejection', ex => {
  throw ex; 
})
*/
