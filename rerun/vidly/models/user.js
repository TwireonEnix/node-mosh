const Joi = require('joi');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    trim: true,
    required: true,
    minlength: 5,
    maxlength: 255
  },
  email: {
    type: String,
    trim: true,
    required: true,
    minlength: 5,
    maxlength: 255,
    unique: true
  },
  password: {
    type: String,
    trim: true,
    required: true,
    minlength: 5,
    maxlength: 255
  },
  isAdmin: {
    type: Boolean,
    default: false
  }
});


/** Adding funciontality to the schema. statics define class functions, and methods instance functions 
 * You can't use here arrow functions because you are binding the word this
 */
userSchema.methods.generateAuthToken = function() {
  return jwt.sign({ _id: this._id, isAdmin: this.isAdmin }, process.env.MOSH_SECRET);
}

const User = mongoose.model('User', userSchema);

const validateUser = user => Joi.validate(user, {
  name: Joi.string().min(5).max(255).required(),
  email: Joi.string().email().min(5).max(255).required(),
  password: Joi.string().min(6).max(70).required()
});

const validateLogin = user => Joi.validate(user, {
  email: Joi.string().email().min(5).max(255).required(),
  password: Joi.string().min(6).max(70).required()
});

module.exports.User = User;
module.exports.validate = validateUser;
module.exports.validateLogin = validateLogin;