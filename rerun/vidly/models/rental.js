const Joi = require('joi');
// /** Way to plugin the objectId validation into joi. Appending a method */
// Joi.objectId = require('joi-objectid')(Joi);
const mongoose = require('mongoose');
const { differenceInDays } = require('date-fns');

const rentalSchema = new mongoose.Schema({
  /** In this example mosh is not using the predefined schema that was created to represent these
   * particular entities. Instead he creates another schema using only the essential properties that
   * are required to solve this particular problem
   */
  customer: {
    type: new mongoose.Schema({
      name: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 50,
      },
      isGold: {
        type: Boolean,
        required: true,
      },
      phone: {
        type: String,
        required: true,
      },
    }),
    required: true,
  },
  movie: {
    type: new mongoose.Schema({
      title: {
        type: String,
        required: true,
        trim: true,
        minlength: 5,
      },
      dailyRentalRate: {
        type: Number,
        required: true,
        min: 0,
        max: 255,
      },
    }),
    required: true,
  },
  dateOut: {
    type: String,
    required: true,
    default: new Date().toISOString(),
  },
  dateReturned: {
    type: String,
  },
  rentalFee: {
    type: Number,
    min: 0,
  }
});

/** Setting a Class method to lookup the rental */
rentalSchema.statics.lookup = function(customerId, movieId) {
  return this.findOne({
    'customer._id': customerId,
    'movie._id': movieId
  });
};

rentalSchema.methods.returnMovie = function() {
  this.dateReturned = new Date().toISOString();
  const rentalDays = differenceInDays(new Date(), new Date(this.dateOut));
  this.rentalFee = rentalDays * this.movie.dailyRentalRate;
}

const Rental = mongoose.model('Rental', rentalSchema);

/** To create a rental is only necessary to provide the customer and the movie respective id's.
 * There's a package to plugin the objectId validation into Joi
 */
const validateRental = rental => Joi.validate(rental, {
  customerId: Joi.objectId().required(),
  movieId: Joi.objectId().required(),
});

module.exports.Rental = Rental;
module.exports.validate = validateRental;