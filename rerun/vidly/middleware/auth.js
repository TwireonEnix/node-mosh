const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
  const token = req.header('x-auth');
  if (!token) return res.status(401).send('Access denied. No token provided.');
  try {
    /** Verify Returns the payload decoded if the signature is valid and optional expiration, audience, 
     * or issuer are valid. If not, it will throw the error.
     * 
     */
    const decoded = jwt.verify(token, process.env.MOSH_SECRET);
    req.user = decoded;
    next();
  } catch (ex) {
    return res.status(400).send('Invalid token');
  }
}