const { ObjectId } = require('mongoose').Types;

module.exports = (req, res, next) => {
  if (!ObjectId.isValid(req.params.id)) return res.status(400).send('Invalid ID');
  next();
}