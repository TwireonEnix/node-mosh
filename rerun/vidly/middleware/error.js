const winston = require('winston');

module.exports = (err, req, res, next) => {
  /** Level of logs
   * Error: Most severe logs
   * Warn: Somthing of care happened but the program still works
   * Info: 
   * Verbose:
   * Debug:
   * Silly
   */
  // winston.log('error', err.message)
  winston.error(err.message, err);
  // Log the exception
  res.status(500).send('Something went wrong');
}