const { Rental } = require('../models/rental');
const { Movie } = require('../models/movies');

module.exports = {
  returnMovie(req, res, next) {
    let savedRental;
    Rental.lookup(req.body.customerId, req.body.movieId).then(rental => {
      if (!rental) return res.status(404).send('Rental not found');
      if (rental.dateReturned) return res.status(400).send('Rental already processed');
      /** Any logic that modifies the state of an object should be encapsulated in the
       * object itself. This is why the logic to set the rental details in the return
       * will be refactored as a instance method in the model. 
       * This is because the Information Expert Principle
       */
      rental.returnMovie();
      rental.save()
        .then(modifiedRental => {
          savedRental = modifiedRental;
          return Movie.updateOne({ _id: rental.movie._id }, { $inc: { numberInStock: 1 } });
        }).then(() => {
          res.send(savedRental);
        });
    }).catch(e => next(e));
  }
};