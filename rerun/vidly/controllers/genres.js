const { Genre, validate } = require('../models/genre');

module.exports = {
  getGenres(req, res, next) {
    Genre.find().sort('name').then(genres => res.send(genres)).catch(err => next(err));
  },

  createGenre(req, res, next) {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    const genre = new Genre({
      name: req.body.name
    });
    genre.save().then(genre => res.send(genre))
      .catch(err => next(err));;
  },

  updateGenre(req, res, next) {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    Genre.findByIdAndUpdate(req.params.id, {
      name: req.body.name
    }, {
      new: true
    }).then(genre => {
      if (!genre) return res.status(404).send('The genre with the given ID was not found.');
      res.send(genre);
    }).catch(err => next(err));
  },

  deleteGenre(req, res, next) {
    Genre.findByIdAndRemove(req.params.id).then(genre => {
      if (!genre) return res.status(404).send('The genre with the given ID was not found.');
      res.send(genre);
    }).catch(err => next(err));
  },

  getGenre(req, res, next) {
    Genre.findById(req.params.id).then(genre => {
      if (!genre) return res.status(404).send('The genre with the given ID was not found.');
      res.send(genre);
    }).catch(err => next(err));
  }

};