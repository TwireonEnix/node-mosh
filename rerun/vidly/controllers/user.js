const { User, validate } = require('../models/user');
const _ = require('lodash');
const bcrypt = require('bcryptjs');

module.exports = {
  register(req, res, next) {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    /** Why would Mosh needed to find if the user already exists if we are validating (although not 
     * strictly a validation according to the docs) in the schema? 
     * */
    let createdUser;
    User.findOne({ email: req.body.email }).then(user => {
        throw new Error('You suck');
        if (user) return res.status(400).send('User already registered');
        return bcrypt.genSalt(10);
      })
      .then(salt => bcrypt.hash(req.body.password, salt))
      .then(hash => {
        req.body.password = hash;
        const registration = new User(_.pick(req.body, ['name', 'email', 'password']));
        return registration.save();
      }).then(newUser => {
        createdUser = newUser;
        /** Signing in users within their registration */
        return createdUser.generateAuthToken();
      }).then(token => {
        res.header('x-auth', token).send(_.pick(createdUser, ['_id', 'name', 'email']))
      }).catch(err => next(err));
  },

  getCurrentUser(req, res, next) {
    User.findById(req.user._id).select('-password').then(user => res.send(user)).catch(err => next(err));
  }

}