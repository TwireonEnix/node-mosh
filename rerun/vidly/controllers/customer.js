const { Customer, validate } = require('../models/customer');

module.exports = {
  getCustomers(req, res, next) {
    Customer.find().sort('name').then(customers => res.send(customers))
      .catch(err => next(err));
  },

  createCustomer(req, res, next) {
    const {
      error
    } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    const customer = new Customer({
      name: req.body.name,
      isGold: req.body.isGold,
      phone: req.body.isGold
    });
    customer.save().then(savedCustomer => res.send(savedCustomer))
      .catch(err => next(err));;
  },

  updateCustomer(req, res, next) {
    const {
      error
    } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    Customer.findByIdAndUpdate(req.params.id, {
      name: req.body.name,
      isGold: req.body.isGold,
      phone: req.body.phone
    }, {
      new: true
    }).then(customer => {
      if (!customer) return res.status(404).send('The customer with the given ID was not found.');
      res.send(customer);
    }).catch(err => next(err));
  },

  deleteCustomer(req, res, next) {
    Customer.findByIdAndRemove(req.params.id).then(customer => {
      if (!customer) return res.status(404).send('The customer with the given ID was not found.');
      res.send(customer);
    }).catch(err => next(err));
  },

  getCustomer(req, res, next) {
    Customer.findById(req.params.id).then(customer => {
      if (!customer) return res.status(404).send('The customer with the given ID was not found.');
      res.send(customer);
    }).catch(err => next(err));
  }

}