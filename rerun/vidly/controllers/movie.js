const { Movie, validate } = require('../models/movies');
const { Genre } = require('../models/genre');

module.exports = {
  getMovies(req, res, next) {
    Movie.find().sort('title').then(movies => res.send(movies))
      .catch(err => next(err));;
  },

  createMovie(req, res, next) {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    Genre.findById(req.body.genreId).then(genre => {
        if (!genre) return res.status(400).send('Invalid genre.');
        const movie = new Movie({
          title: req.body.title,
          /** The next line is important to deal with a hybrid reference approach (select individual properties to include) */
          genre: {
            _id: genre._id,
            name: genre.name
          },
          numberInStock: req.body.numberInStock,
          dailyRentalRate: req.body.dailyRentalRate
        });
        return movie.save();
      }).then(createdMovie => res.send(createdMovie))
      .catch(err => next(err));
  },

  updateMovie(req, res, next) {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    Genre.findById(req.body.genreId).then(genre => {
      if (!genre) return res.status(400).send('Invalid genre.');
      return Movie.findOneAndUpdate(req.params.id, {
        title: req.body.title,
        genre: {
          _id: genre._id,
          name: genre.name
        },
        numberInStock: req.body.numberInStock,
        dailyRentalRate: req.body.dailyRentalRate
      }, {
        new: true
      });
    }).then(updatedMovie => {
      if (!updatedMovie) return res.status(404).send('The movie with the given ID was not found');
      res.send(movie);
    }).catch(err => next(err));;
  },

  deleteMovie(req, res, next) {
    Movie.findByIdAndRemove(req.params.id).then(movie => {
      if (!movie) return res.status(404).send('The movie with the given ID was not found');
      res.send(movie);
    }).catch(err => next(err));
  },

  getMovie(req, res, next) {
    Movie.findById(req.params.id).then(movie => {
      if (!movie) return res.status(404).send('The movie with the given ID was not found');
      res.send(movie);
    }).catch(err => next(err));
  },
}