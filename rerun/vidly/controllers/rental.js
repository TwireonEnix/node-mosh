const { Rental, validate } = require('../models/rental');
const { Movie } = require('../models/movies');
const { Customer } = require('../models/customer');

module.exports = {
  getRentals(req, res, next) {
    /** sorting by dateIn a desc Order */
    Rental.find().sort('-dateOut').then(rentals => res.send(rentals)).catch(err => next(err));
  },

  createRental(req, res, next) {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    let customer, movie;
    Customer.findById(req.body.customerId).then(fetchedCustomer => {
      if (!fetchedCustomer) return res.status(400).send('Invalid customer');
      customer = fetchedCustomer;
      return Movie.findById(req.body.movieId);
    }).then(fetchedMovie => {
      if (!fetchedMovie) return res.status(400).send('Invalid movie');
      if (fetchedMovie.numberInStock === 0) return res.status(400).send('Movie not available');
      movie = fetchedMovie;
      /** Same approach to create a hybrid balance between normalization and denormalization. This
       * helps to improve performance as it reduces the operations needed to query the documents, but
       * it duplicates some data.
       */
      const rental = new Rental({
        customer: {
          _id: customer._id,
          name: customer.name,
          isGold: customer.isGold,
          phone: customer.phone
        },
        movie: {
          _id: movie._id,
          title: movie.title,
          dailyRentalRate: movie.dailyRentalRate
        }
      });

      /** Here we need to update the movie existences document with the approach of movie.save(), but also need to 
       * write the rental correctly to make the change, meaning these operation need to be performed together in
       * an atomic way. In other words, a (sql) transaction (group of operations that must be treated as a unit, 
       * either all completes or all fail and db is not affected).
       * There's this npm pckg that under the hood implement mongodb two phase commits. It' called fawn.
       * 
       * Operations pending:
       * 1) Save movie document with -1 availability
       * 2) Save the rental document to db
       * 
       * One cannot be completed without the other.
       * 
       * (note): Fawn is no lorger supported nor needed because since mongoose 5 transactions are supported;
       * The next lines are extracted from the mongoose docs for transactions. (how can i test that the rollback is
       * made correctly if there was an error?)
       */
      let newRental, session;
      Rental.startSession().then(currentSession => {
        session = currentSession;
        session.startTransaction()
        return rental.save();
      }).then(savedRental => {
        newRental = savedRental;
        movie.numberInStock--;
        return movie.save();
      }).then(() => {
        session.commitTransaction().then(() => {
          session.endSession();
          res.send(newRental);
        });
      }).catch(err => {
        session.abortTransaction().then(() => session.endSession());
        next(err)
      });
    });
  },

  updateRental(req, res) {},

  deleteRental(req, res) {},

  getRental(req, res) {},
};