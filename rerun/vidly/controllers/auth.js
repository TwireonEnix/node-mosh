const { User, validateLogin } = require('../models/user');
const bcrypt = require('bcryptjs');

module.exports = {
  login(req, res, next) {
    const { error } = validateLogin(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    let fetchedUser;
    User.findOne({ email: req.body.email }).then(user => {
        if (!user) return res.status(400).send('Invalid email or password');
        fetchedUser = user;
        return bcrypt.compare(req.body.password, user.password);
      }).then(validPassword => {
        if (!validPassword) return res.status(400).send('Invalid email or password');
        const payload = {
          _id: fetchedUser._id
        }
        return fetchedUser.generateAuthToken();
      }).then(token => res.send(token))
      .catch(err => next(err));
  }
}