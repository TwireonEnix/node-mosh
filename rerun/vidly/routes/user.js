const userController = require('../controllers/user');
const auth = require('../middleware/auth');
const express = require('express');
const router = express.Router();

router.post('/', userController.register);
router.get('/me', auth, userController.getCurrentUser);

module.exports = router;