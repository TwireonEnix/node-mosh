const rentalController = require('../controllers/rental');
const express = require('express');
const router = express.Router();

router.get('/', rentalController.getRentals);
router.post('/', rentalController.createRental);

module.exports = router;