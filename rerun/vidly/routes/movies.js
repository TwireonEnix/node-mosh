const movieController = require('../controllers/movie');
const express = require('express');
const router = express.Router();

router.get('/', movieController.getMovies);
router.post('/', movieController.createMovie);
router.put('/:id', movieController.updateMovie);
router.delete('/:id', movieController.deleteMovie);
router.get('/:id', movieController.getMovie);

module.exports = router;