const returnsController = require('../controllers/returns');
const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const validator = require('../middleware/validator');
const { validate } = require('../models/rental');

router.post('/', auth, validator(validate), returnsController.returnMovie);

module.exports = router;