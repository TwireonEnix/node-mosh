const auth = require('../middleware/auth');
const admin = require('../middleware/admin');
const validId = require('../middleware/validId');
const genresController = require('../controllers/genres');
const express = require('express');
const router = express.Router();

router.get('/', genresController.getGenres);
router.post('/', auth, genresController.createGenre);
router.put('/:id', validId, genresController.updateGenre);
router.delete('/:id', validId, auth, admin, genresController.deleteGenre);
router.get('/:id', validId, genresController.getGenre);

module.exports = router;