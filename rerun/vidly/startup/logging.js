const winston = require('winston');
// require('winston-mongodb');

module.exports = () => {
  // const connectionString = `mongodb+srv://${process.env.MOSH_REMOTE_DB_USER}:${process.env.MOSH_REMOTE_DB_PASS}@${process.env.MOSH_REMOTE_DB_HOST}/moshVidly?retryWrites=true01`;
  /** Winston is a logger, it comes with diverse transports, which are the methods
   * that logs to diffent endpoints like console, databases, files, http, etc.
   * Winston can handle the same of above setting a new transport, so when handling
   * promise rejections we simply throw and exception.
   */

  process.on('unhandledRejection', ex => {
    // console.log('we got an unhandled rejection');
    // winston.error(ex.message, ex);
    // process.exit(1);
    throw ex;
  })
  winston.exceptions.handle(
    new winston.transports.Console({ format: winston.format.simple() }),
    new winston.transports.File({ filename: 'uncaughtExceptions.log ' }));

  /** Winston is a logger, it comes with diverse transports, which are the methods
   * that logs to diffent endpoints like console, databases, files, http, etc.
   */

  /* Handeled by winston.exceptions.handle();
  process.on('uncaughtException', ex => {
    console.log('we got an uncaught exception');
    winston.error(ex.message, ex);
    //As a best practice it's better to exit the program whenever an error occurs 
    process.exit(1);
  })
  */
  /** Logging here with winston only works in the context of express, only in the
   * process request pipeline
   */
  winston.add(new winston.transports.File({ filename: 'messages.log' }));
  // winston.add(new winston.transports.MongoDB({
  //   db: connectionString,
  //   level: 'warn',
  // }));
}