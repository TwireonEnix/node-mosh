/** In a refactor here we define the routes without creating a new
 * express app, but we need the app object because these routes will be
 * declared within the app.
 */
const express = require('express');
const error = require('../middleware/error');
const genres = require('../routes/genres');
const customers = require('../routes/customers');
const movies = require('../routes/movies');
const rental = require('../routes/rental');
const user = require('../routes/user');
const auth = require('../routes/auth');
const returns = require('../routes/returns');
const morgan = require('morgan');

module.exports = app => {

  if (app.get('env') === 'development') app.use(morgan('dev'));
  app.use(express.json());
  app.use('/api/genres', genres);
  app.use('/api/customers', customers);
  app.use('/api/movies', movies);
  app.use('/api/rental', rental);
  app.use('/api/user', user);
  app.use('/api/auth', auth);
  app.use('/api/returns', returns);
  /** Error handling middleware */
  app.use(error);
}