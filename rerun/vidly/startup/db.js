const mongoose = require('mongoose');
const winston = require('winston');

mongoose.Promise = global.Promise;
mongoose.set('useCreateIndex', true);

module.exports = () => {
  /** Main db */
  let db = 'moshVidly'
  /** When running tests the environment is set to test */
  if (process.env.NODE_ENV === 'test') db = 'vidly_tests';
  const connectionString = `mongodb+srv://${process.env.MOSH_REMOTE_DB_USER}:${process.env.MOSH_REMOTE_DB_PASS}@${process.env.MOSH_REMOTE_DB_HOST}/${db}?retryWrites=true01`;
  mongoose.connect(connectionString, {
      useNewUrlParser: true
    })
    .then(() => winston.info(`Connected to Atlas and ${db}`))
    .catch(err => winston.error(err));
};