/** Test driven development
 * Tests for /api/returns
 * 
 * Possibilities:
 * (negative cases)
 * Return 401 if client is not logged in
 * Return 400 if customerId is not provided
 * Return 400 if movieId is not provided
 * Return 404 if no rental found for this customer/movie
 * Return 400 if rental already processed
 * 
 * (positive case)
 * Return 200 if valid
 * Set the return date
 * Calculate rental fee
 * Increase the stock
 * Return the rental
 * 
 */
const request = require('supertest');
const { expect } = require('chai');
const { Rental } = require('../../models/rental');
const { Movie } = require('../../models/movies');
const { User } = require('../../models/user');
const { ObjectId } = require('mongoose').Types;
const { addDays } = require('date-fns');

describe('POST /api/returns', () => {
  let app, customerId, movieId, rental, payload, token, movie;

  /** Defining the happy path with mosh's techique. After that in each case we will
   * change one parameter
   */
  const execute = () => {
    return request(app).post('/api/returns').set('x-auth', token).send(payload);
  }

  beforeEach(done => {
    app = require('../../index');
    customerId = new ObjectId();
    movieId = new ObjectId();
    token = new User().generateAuthToken();

    movie = new Movie({
      _id: movieId,
      title: '12345',
      dailyRentalRate: 2,
      genre: { name: '12345' },
      numberInStock: 9
    });

    rental = new Rental({
      customer: {
        _id: customerId,
        name: '12345',
        phone: '12345',
        isGold: true
      },
      movie: {
        _id: movieId,
        title: '12345',
        dailyRentalRate: 2
      }
    });
    payload = { customerId, movieId };
    movie.save().then(() => rental.save()).then(() => done());
  });

  afterEach(done => {
    app.close();
    Rental.deleteMany({}).then(() => Movie.deleteMany({})).then(() => done());
  });

  it('should make sure rental is in db', done => {
    Rental.findById(rental._id).then(result => {
      // expect(result).not.toBeNull();
      expect(result).to.exist;
      done();
    });
  });

  it('should return 401 if client is not logged in', done => {
    token = '';
    execute().expect(401).end(done);
  });

  it('should return 400 if customerId is not provided', done => {
    delete payload.customerId;
    execute()
      .expect(400).end(done);
  });

  it('should return 400 if movieId is not provided', done => {
    delete payload.movieId;
    execute()
      .expect(400).end(done);
  });

  it('should return 404 if movie/customer not found', done => {
    payload.movieId = new ObjectId();
    payload.customerId = new ObjectId();
    execute().expect(404).end(done);
  });

  it('should return 400 if rental was already processed', done => {
    rental.dateReturned = new Date().toISOString();
    rental.save().then(() => {
      execute().expect(400).end(done);
    });
  });

  it('should return 200 if request is successful', done => {
    execute().expect(200).end(done);
  });

  it('should set the returnDate if input is valid', done => {
    execute().expect(200).end((err, res) => {
      Rental.findById(rental._id).then(rentalInDB => {
        /** This assertion is too general so we'll test if the dateReturned is set
         * within a 10 seconds range (worst case scenario) from the current time
         */
        expect(rentalInDB.dateReturned).to.exist;
        const diff = new Date() - new Date(rentalInDB.dateReturned);
        expect(diff).to.be.below(10 * 1000);
        done();
      });
    });
  });

  it('should set the rental fee if input is valid', done => {
    /** Setting the date 7 days before calling the execute */
    rental.dateOut = addDays(new Date(), -7).toISOString();
    rental.save().then(() => {
      execute().expect(200).end((err, res) => {
        Rental.findById(rental._id).then(rentalInDB => {
          expect(rentalInDB.rentalFee).to.exist;
          expect(rentalInDB.rentalFee).to.equal(14);
          done();
        });
      });
    });
  });

  it('should increase the movie stock if input is valid', done => {
    execute().expect(200).end((err, res) => {
      Movie.findById(movieId).then(movieInDb => {
        expect(movieInDb.numberInStock).to.equal(movie.numberInStock + 1);
        done();
      });
    });
  });

  it('should return rental if input is valid', done => {
    execute().expect(200).end((err, res) => {
      Rental.findById(rental._id).then(rentalInDB => {
        expect(res.body).to.contain.any.keys(['dateOut', 'dateReturned', 'rentalFee', 'customer', 'movie']);
        done();
      });
    });
  });

})