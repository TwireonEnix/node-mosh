const request = require('supertest');
const { Genre } = require('../../models/genre');
// Required to login and get an auth token
const { User } = require('../../models/user');
const expect = require('expect');

describe('/api/genres', () => {
  let server;
  server = require('../../index');
  beforeEach(done => {
    Genre.deleteMany({}).then(() => done());
  });
  afterEach(done => {
    server.close();
    done();
  });

  describe('GET /', () => {

    /** Define the happy path, and in each test, we change one parameter that clearly aligns 
     * with the name of the test
     */

    it('should return all genres', done => {
      Genre.collection.insertMany([
        { name: 'genre1' },
        { name: 'genre2' },
      ]);
      request(server).get('/api/genres').then(res => {
        expect(res.status).toBe(200);
        expect(res.body.length).toBe(2);
        expect(res.body.some(g => g.name === 'genre1')).toBeTruthy();
        expect(res.body.some(g => g.name === 'genre2')).toBeTruthy();
        done();
      });
    });
  });

  describe('GET /:id', () => {
    it('should return a genre if valid id is passed', done => {
      const genre = new Genre({
        name: 'genre1'
      });
      genre.save().then(newGenre => {
        request(server).get(`/api/genres/${newGenre._id.toHexString()}`).then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toHaveProperty('name', genre.name);
          done();
        });
      });
    });

    it('should return 400 if invalid id is passed', done => {
      request(server).get('/api/genres/1').expect(400).end(done);
    })
  });

  describe('POST /', () => {
    it('should return 401 if client is not logged in', done => {
      request(server).post('/api/genres').send({ name: 'genre1' }).expect(res => {
        expect(res.status).toBe(401);
      }).end(done);
    });
  })

  it('should return 400 if genre is less than 5 characters', done => {
    const token = new User().generateAuthToken();
    request(server).post('/api/genres').set('x-auth', token).send({ name: '1234' })
      .expect(res => {
        expect(res.status).toBe(400);
      }).end(done);
  });

  it('should return 400 if genre is greater than 5 characters', done => {
    const token = new User().generateAuthToken();
    request(server).post('/api/genres').set('x-auth', token).send({ name: new Array(52).join('a') })
      .expect(res => {
        expect(res.status).toBe(400);
      }).end(done);
  });

  it('should create a new genre if input is valid', done => {
    const token = new User().generateAuthToken();
    request(server).post('/api/genres').set('x-auth', token).send({ name: 'new Genre' })
      .expect(res => {
        expect(res.status).toBe(200);
        return Genre.findOne({ name: 'new Genre' })
      })
      .then(doc => {
        expect(doc).not.toBeNull();
        done();
      })
  });

  it('should return a new genre if input is valid', done => {
    const token = new User().generateAuthToken();
    request(server).post('/api/genres').set('x-auth', token).send({ name: 'new Genre' })
      .expect(res => {
        expect(res.status).toBe(200);
        expect(res.body).toHaveProperty('_id');
        expect(res.body).toHaveProperty('name', 'new Genre');
      }).end(done);
  });


});