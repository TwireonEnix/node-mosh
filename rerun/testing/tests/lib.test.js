const expect = require('chai').expect;
const lib = require('../lib');


/** General rule of unit testing is that the number of tests required for a 
 * function is greater than or equal to the number of the execution path in
 * that function
 */
describe('Testing the lib.js collection of functions', () => {

  describe('absolute', () => {

    it('should return a positive number if input is positive', () => {
      const result = lib.absolute(1);
      expect(result).to.equal(1);
    });
    it('should return a positive number if input is negative', () => {
      const result = lib.absolute(-1);
      expect(result).to.equal(1);
    });
    it('should return 0 if input is 0', () => {
      const result = lib.absolute(0);
      expect(result).to.equal(0);
    });
  });

  describe('greet', () => {
    /** This test is too specific, test should not be neither too specific nor
     * too general
     */
    it('Should return the greeting message', () => {
      const result = lib.greet('Darío');
      /** To be less specific in this test we will just look for the arg string
       * in the general message with a regex
       */
      // expect(result).to.equal('Welcome Darío');
      expect(result).to.match(/Darío/);
      /** An easier way without using regex */
      expect(result).to.contain('Darío');
    });
  })

  describe('getCurrencies', () => {
    it('should return supported currencies', () => {
      const result = lib.getCurrencies();

      /** Too general ways in testing arrays */
      expect(result).to.exist;
      expect(result).to.not.undefined;
      /** Too specific */
      expect(result[0]).to.equal('USD');
      expect(result[1]).to.equal('AUD');
      expect(result[2]).to.equal('EUR');
      expect(result).to.have.lengthOf(3);

      /** Proper way: To contain the object */
      expect(result).to.contain('USD');
      expect(result).to.contain('AUD');
      expect(result).to.contain('EUR');
    });
  });

  describe('getProduct', () => {
    it('should return the product with the given id', () => {
      const result = lib.getProduct(1);
      /** To include allows the test to not be too general nor too
       * specific by searching the only kvps we are interested in, deep
       * is used because equal is strict equality which will always fail
       * in objects because of how they are declared in memory
       */
      expect(result).to.deep.include({ id: 1, price: 10 });
    });
  })

  describe('registerUser', () => {
    it('should throw an error if username is falsy', () => {
      /** False values: null, undefined, nan, '', 0, false 
       * To test for exceptions we expect to test a function with a callback
       */
      const args = [null, undefined, NaN, '', 0, false]
      for (const x of args) {
        expect(() => lib.registerUser(x)).to.throw();
      }
    });
    it('should return a user object if valid username is passed', () => {
      const result = lib.registerUser('User');
      expect(result).to.deep.include({ username: 'User' });
      expect(result.id).to.greaterThan(0);
    });
  });

});