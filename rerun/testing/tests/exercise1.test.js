const lib = require('../exercise1');
const expect = require('chai').expect;

describe('fizzBuzz', () => {
  it('should throw an exception if input is nan', () => {
    expect(() => { lib.fizzBuzz('a') }).to.throw();
    expect(() => { lib.fizzBuzz(undefined) }).to.throw();
    expect(() => { lib.fizzBuzz(null) }).to.throw();
    expect(() => { lib.fizzBuzz({}) }).to.throw();
  });

  it('should return FizzBuzz if input is divisible by 3 and 5', () => {
    const result = lib.fizzBuzz(15);
    expect(result).to.equal('FizzBuzz');
  })
  it('should return FizzBuzz if input is only divisible by 3', () => {
    const result = lib.fizzBuzz(3);
    expect(result).to.equal('Fizz');
  });
  it('should return FizzBuzz if input is only divisible by 5', () => {
    const result = lib.fizzBuzz(5);
    expect(result).to.equal('Buzz');
  });
  it('should return input if input is not divisible by 3 or 5', () => {
    const result = lib.fizzBuzz(1);
    expect(result).to.equal(1);
  });
});