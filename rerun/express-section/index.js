const denv = require('dotenv').config();
if (!denv) return;

/** Using the debugging function to log color coded information in the console
 * This has to be set in env variables with DEBUG=app:namespace
 */
const startupDebugger = require('debug')('app:startup');
const dbDebugger = require('debug')('app:db');


const logger = require('./middleware/logger');
const helmet = require('helmet');
const morgan = require('morgan');
const config = require('config');

const coursesRoutes = require('./routes/courses.routes');

const express = require('express');
const app = express();

/** 
 * Request processing pipeline.
 * Middleware functions: In express a middleware function takes control of the request 
 * in the process request pipeline, performing certain logic over it and finally 
 * either passing the control to another middleware of sending a response to the client.
 * 
 * In express every route handler is technically a middleware
 * 
 * -- Request --> json() --> route() --> response -->
 * 
 * In every custom middleware function we write, if next is not called the request will
 * hang and it will never be fulfilled.
 * 
 * Middleware functions are called in sequence.
 * 
 */

app.use(logger);

/** Piece of middleware to be able to parse the body of the request as json */
app.use(express.json());
/** Helmet is a third party middleware that modify response headers to help ensuring a secure
 * environment. According to the docs, helmet is a collection of several smaller middlewares
 */
app.use(helmet());

/** Morgan is a logger which can be configured to log to console or other resources, in this
 * case we will enable it only if the environment is set to development (which is the default)
 */
if (app.get('env') === 'development') {
  app.use(morgan('tiny'));
  startupDebugger('Morgan enabled...');
  dbDebugger('Connected to db');
}

/** enables x-www-form-urlencoded into a req.body. the extended property allows the
 * parsing of more complex objects like arrays or files.
 */
app.use(express.urlencoded({
  extended: true
}));

/** Enables express to serve static assets given a specific root path, static content
 * is served from the root of the site ie (localhost:3000/somefile.txt)
 */
app.use(express.static('public'));

app.use((req, res, next) => {
  console.log('Authenticating..');
  next();
});

/** Configuration */
console.log(`App name: ${config.get('name')}`);
console.log(`Mail server: ${config.get('mail.host')}`);
console.log(`Mail password: ${config.get('mail.password')}`);



/** Using the imported routes from another module. This tells express that for every
 * route starting in /api/courses, use this router. Then the routes can be refactored
 * inside the file to be shorter
 */
app.use('/api/courses', coursesRoutes);

const port = process.env.PORT || 3000;

/** Way to check the node environment. If the env is not set, app.get('env') is
 * set to development
  console.log(`NODE_ENV ${process.env.NODE_ENV}`);
  */
console.log(`app: ${app.get('env')}`);

app.listen(port, () => console.log(`Server on ${port}`));