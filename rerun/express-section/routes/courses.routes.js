const express = require('express');
const Joi = require('joi');
/** In express apps we work with routers within the routing files */
const router = express.Router();

// router.get('/', (req, res) => {
//   console.log(req.baseUrl);
//   res.send('Hello World');
// });

let courses = [{
    id: 1,
    name: 'Web design'
  },
  {
    id: 2,
    name: 'NodeJS'
  },
  {
    id: 3,
    name: 'VueJS'
  },
];

router.get('/', (req, res) => {
  res.send(courses);
});

router.get('/:id', (req, res) => {
  /** array method to find an especific object */
  const course = courses.find(course => course.id === +req.params.id);
  if (!course) return res.status(404).send('Course not found');
  res.status(200).send(course);
});

router.post('/', (req, res) => {

  /** Definition of a schema to be validated with joi */
  const schema = {
    name: Joi.string().min(3).required()
  };
  const result = Joi.validate(req.body, schema);
  // console.log(result);
  // 400 bad request
  if (result.error) return res.status(400).send(result.error.details[0].message);

  const course = {
    id: courses.length + 1,
    name: req.body.name
  };
  courses.push(course);
  res.send(course);
});

router.put('/:id', (req, res) => {
  const course = courses.find(course => course.id === +req.params.id);
  if (!course) return res.status(404).send('Course not found');

  const {
    error
  } = validateCourse(req.body);
  if (error) return res.status(400).send(error.details[0].message);
  course.name = req.body.name;
  res.status(200).send(course);
});

router.delete('/:id', (req, res) => {
  const course = courses.find(course => course.id === +req.params.id);
  if (!course) return res.status(404).send('Course not found');
  courses = courses.filter(course => course.id !== +req.params.id);
  res.status(200).send(course);
});

/** Extracting the validation login to refactor */
const validateCourse = course => {
  const schema = {
    name: Joi.string().min(3).required()
  };
  return Joi.validate(course, schema);
}

module.exports = router;