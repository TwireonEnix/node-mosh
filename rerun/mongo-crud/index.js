require('dotenv').config();
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

const connectionString = `mongodb+srv://${process.env.MOSH_REMOTE_DB_USER}:${process.env.MOSH_REMOTE_DB_PASS}@${process.env.MOSH_REMOTE_DB_HOST}/moshNode?retryWrites=true01`;

mongoose.connect(connectionString, {
    useNewUrlParser: true
  })
  .then(() => console.log('Connected')).catch(err => console.log(err));

const courseSchema = new mongoose.Schema({
  name: String,
  author: String,
  tags: [String],
  date: {
    type: Date,
    default: Date.now
  },
  isPublished: Boolean
});


/** Pascal naming convetion because it's a class, not an object */
const Course = mongoose.model('Course', courseSchema);

// const course = new Course({
//   name: 'C# Course',
//   author: 'Mosh',
//   tags: ['c#', 'backend'],
//   isPublished: true
// });

// const course = new Course({
//   name: 'Vue Js Course',
//   author: 'Max',
//   tags: ['frontend', 'vue'],
//   isPublished: true
// });

// course.save().then(doc => console.log('saved', doc));
// Course.find().then(docs => console.log(docs));

Course.find({
    author: 'Mosh'
  })
  .limit(10)
  .sort({
    name: 1
  })
  .select({
    name: 1,
    tags: 1
  })
  .then(courses => console.log(courses));