require('dotenv').config();
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

const connectionString = `mongodb+srv://${process.env.MOSH_REMOTE_DB_USER}:${process.env.MOSH_REMOTE_DB_PASS}@${process.env.MOSH_REMOTE_DB_HOST}/moshNode?retryWrites=true01`;

mongoose.connect(connectionString, {
    useNewUrlParser: true
  })
  .then(() => console.log('Connected')).catch(err => console.log(err));


const authorSchema = new mongoose.Schema({
  name: String,
  bio: String,
  website: String
});

const Author = mongoose.model('Author', authorSchema);

/** Embedding another document */
const Course = mongoose.model('Course', new mongoose.Schema({
  name: String,
  authors: [authorSchema]
}));

const createAuthor = (name, bio, website) => {
  const author = new Author({
    name,
    bio,
    website
  });
  author.save().then(result => console.log(result));
}

const createCourse = (name, authors) => {
  const course = new Course({
    name,
    authors
  });
  course.save().then(newDoc => console.log(newDoc));
}

const listCourses = () => {
  Course.find()
    .populate('author', 'name -_id')
    .select('name author').then(courses => console.log(courses));
}

const updateAuthor = (courseId) => {
  Course.findById(courseId).then(doc => {
    doc.author.name = 'Darío Navarrete';
    return doc.save();
  }).then(updated => {
    console.log('updated', updated);
  })
}

/* Shorter version*/
const updateAuthorB = courseId => {
  Course.findByIdAndUpdate(courseId, {
    author: {
      name: 'Darío A secas'
    }
  }, {
    new: true
  }).then(doc => console.log(doc));
}

/** Mosh version */

const updateAuthorM = courseId => {
  Course.update({
    _id: courseId
  }, {
    $set: {
      'author.name': 'John Smith'
    }
  }).then(doc => console.log(doc));
}

const removeAuthor = _id => {
  Course.updateOne({
    _id
  }, {
    $unset: {
      author: ''
    }
  }).then(doc => console.log(doc));
}
// createAuthor('Darío', 'Javascript guru', 'dar.io');
createCourse('Vue JS', [
  new Author({
    name: 'Darío'
  }),
  new Author({
    name: 'Mosh'
  })
]);
// listCourses();
// removeAuthor('5c5f30789efaa1107f8c49f4');