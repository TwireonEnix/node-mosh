require('dotenv').config();
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

const connectionString = `mongodb+srv://${process.env.MOSH_REMOTE_DB_USER}:${process.env.MOSH_REMOTE_DB_PASS}@${process.env.MOSH_REMOTE_DB_HOST}/moshNode?retryWrites=true01`;

mongoose.connect(connectionString, {
    useNewUrlParser: true
  })
  .then(() => console.log('Connected')).catch(err => console.log(err));

const Author = mongoose.model('Author', new mongoose.Schema({
  name: String,
  bio: String,
  website: String
}));

/** Referencing another document */
const Course = mongoose.model('Course', new mongoose.Schema({
  name: String,
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Author'
  }
}));

const createAuthor = (name, bio, website) => {
  const author = new Author({
    name,
    bio,
    website
  });
  author.save().then(result => console.log(result));
}

const createCourse = (name, author) => {
  const course = new Course({
    name,
    author
  });
  course.save().then(newDoc => console.log(newDoc));
}

const listCourses = () => {
  Course.find()
    .populate('author', 'name -_id')
    .select('name author').then(courses => console.log(courses));
}

// createAuthor('Darío', 'Javascript guru', 'dar.io');
// createCourse('Vue JS', '5c5f2d572998830ed26f7bb1');
listCourses();