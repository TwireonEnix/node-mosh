/* notas de versiones:

"mongoose": "^5.2.6", --- Caret caracter ^ significa que siempre se usará la versión major (semantic versioning) Major.Minor.Patch
"underscore": "~1.9.1" -- ~ La tilde significa que se usarla la version major y minor

para instalar versiones específicas de los npm mongoose@x.x.x
npm view 'paquete' (sección)

npm outdated para ver los paquetes más nuevos, los requeridos y los actuales

npm check updates para revisar las dependencias e instalar las ultimas versiones

ncu despues de instalar de manera global en npm-check-updates
ncu -u para actualizar el package.json y npm i para instalar las últimas versiones de las dependencias

Development Dependecies, no van en las dependncias de producción --save-dev
Desinstalar dependencias npm un underscore
*/


const http = require('http');
const app = require('./app.js');

const port = process.env.PORT || 3000;
app.set('port', port);
const server = http.createServer(app);
server.listen(port, () => {
  console.log(`Server running on port ${port}`);
});