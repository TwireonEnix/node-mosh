const startUpDebugger = require('debug')('app:startup');
const dbDebugger = require('debug')('app:db');
const config = require('config');
const helmet = require('helmet');
const morgan = require('morgan');
const express = require("express");
const app = express();
const joi = require("joi");



/* config es una manera de llamar variables de entorno del sistema, sin embargo aquí no me
funciona. igualmente los debuggers al utilizar una variable de entorno, NO FUNCIONAN */

console.log('Application Name: ' + config.get('name'));
console.log('Mail Server: ' + config.get('mail.host'));
// console.log('Mail pwd: ' + config.get('mail.password'));

/* El debugger funciona mandando console.logs de lo que sea que se esté debuggeando,
sin embargo funciona al modificar el startup de la siguiente manera en package.json
    "start": "DEBUG=app:db nodemon index.js"*/
startUpDebugger('hola');
dbDebugger('connected to db..');

/* Middleware: funciones que reciben las peticiones y las transforman o las analizan
de cierta manera, usando un next para pasar el control de la petición al siguiente
middleware. entre más middlewares tenga la aplicación, más lenta se volverá. */


/* lo que hacía body-parser haciéndolo innecesario ya. El express.json() es un
middleware que parsea los */
app.use(express.json());

/* para que se puedan mandar valores key-value dentro del body en un form
con el extended se pueden pasar elementos más complejos */
app.use(express.urlencoded({extended: true}));

/* sirve para servir archivos estáticos desde un folder predefinido en el parámetro
se abriran los archivos desde el root escribiendo localhost:3000/readme.txt */
app.use(express.static('public'));

/* Helmet da funciones de seguridad, y proteje la app de vulnerabilidades configurando
las cabeceras de http correctamente */
app.use(helmet());

/* Morgan da un logger de todas las peticiones que le llegan al server */
app.use(morgan('tiny'));

const courses = [
  {
    id: 1,
    name: "course 1"
  },
  {
    id: 2,
    name: "course 2"
  },
  {
    id: 3,
    name: "course 3"
  }
];

app.get("/", (req, res) => {
  res.send("hola");
});

app.get("/api/courses", (req, res) => {
  res.send(courses);
});

app.get("/api/courses/:id", (req, res) => {
  /* el método find está disponible out of the box para todos los arreglos de javascript 
  así como los parseos */
  const course = courses.find(c => c.id === parseInt(req.params.id));
  if (!course)
    res.status(404).json({
      message: "There is no course with this id"
    });
  res.json({
    course: course
  });
});

app.post("/api/courses/new", (req, res) => {
  /* Para usar la validación del body con joi, se define un esquema del objeto que se espera recibir y se valida
  con los métodos inherentes al paquete (ver documentación de joi para más info) */
  const joiSchema = {
    name: joi
      .string()
      .min(3)
      .required()
  };
  /* Se guarda en una constante el resultado del método validar pasándole como parámetros*/
  const result = joi.validate(req.body, joiSchema);
  console.log(result);
  if (result.error) {
    return res.status(400).json({ error: result.error.details[0].message });
  }
  const courseData = {
    id: courses.length + 1,
    name: req.body.name
  };
  console.log(courseData);
  courses.push(courseData);
  res.send(courses);
});

app.put("/api/courses/:id", (req, res) => {
  /* Look up the course */
  const course = courses.find(cou => cou.id === +req.params.id);
  /* If it does not exist, return 404 */
  if (!course) return res.status(404).json({ message: "Course not found" });
  /* Validate: creando una nueva función dentro del módulo que hace la validación */
  /* el equivalente de obtener result.error, esto se le llama object destructuring*/
  const { error } = validateCourse(req.body);
  /* If invalid, return 400 - bad request */
  if (error)
    return res
      .status(400)
      .json({ message: "Bad request", error: error.details });
  /* Update course */
  course.name = req.body.name;
  /* Return updated course */
  res.send(course);
});

app.delete("/api/courses/:id", (req, res) => {
  /* Look up the course */
  const course = courses.find(cou => cou.id === +req.params.id);
  /* If it does not exist, return 404 */
  if (!course) return res.status(404).json({ message: "Course not found" });
  /* delete: Encontrar primero el índice del objeto dentro del arreglo */
  const index = courses.indexOf(course);
  /* Eliminar el objeto con el método splice que recibe el índice y el num de elementos a remover */
  courses.splice(index, 1);
  /* return deleted object */
  res.send(course);
});

function validateCourse(course) {
  const joiSchema = {
    name: joi
      .string()
      .min(3)
      .required()
  };
  return (isValid = joi.validate(course, joiSchema));
}

module.exports = app;
