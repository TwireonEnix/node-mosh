const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/playground')
  .then(() => console.log('Connected to MongoDB...'))
  .catch(err => console.error('Could not connect to MongoDB...', err));

const Author = mongoose.model('Author', new mongoose.Schema({
  name: String,
  bio: String,
  website: String
}));

const Course = mongoose.model('Course', new mongoose.Schema({
  name: String,
  author: {
    /* Para hacer referencias como tal se utiliza el tipo de dato objectid de mongoose, y 
    la referencia que es el nombre de la colección de donde procede el dato. */
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Author'
  }
}));

async function createAuthor(name, bio, website) { 
  const author = new Author({
    name,
    bio,
    website
  });

  const result = await author.save();
  console.log(result);
}

async function createCourse(name, author) {
  const course = new Course({
    name, 
    author
  }); 
  
  const result = await course.save();
  console.log(result);
}

async function listCourses() {
  const courses = await Course
    .find()
    /* método populate para llenar los queries con las referencias del objectid, cuando únicamente se pasa el campo
    donde se quiere hacer la llamada a la otra colección, ésta regresará el documento completo. Para seleccionar las
    propiedades que se quieran incluir se pasan como segundo parámetro al método populate */
    .populate('author', ['name', 'bio'])
    .select('name author');
  console.log(courses);
}

//createAuthor('Mosh', 'My bio', 'My Website');

// createCourse('Node Course', '5b6b006eab21f638a455c009');

listCourses();