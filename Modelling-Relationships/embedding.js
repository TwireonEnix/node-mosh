const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/playground')
  .then(() => console.log('Connected to MongoDB...'))
  .catch(err => console.error('Could not connect to MongoDB...', err));

const authorSchema = new mongoose.Schema({
  name: { type: String, required: true },
  bio: String,
  website: String
});

const Author = mongoose.model('Author', authorSchema);

const Course = mongoose.model('Course', new mongoose.Schema({
  name: String,
  /* Aquí en lugar de hacer una 'relación' con objectId, para embeber un documento dentro de otro, se
  pasa como tipo de dato el esquema de mongoose previamente definido */
  // author: { type: authorSchema, required: true }

  /* Embeber un arreglo de documentos en un documento:  */
  authors: [authorSchema]
}));

async function createCourse(name, authors) {
  const course = new Course({
    name, 
    authors
  }); 
  
  const result = await course.save();
  console.log(result);
}

async function listCourses() { 
  const courses = await Course.find();
  console.log(courses);
}

async function updateAuthor(courseId) {
  /* Forma 1: 
  const course = await Course.findById(courseId);
  course.author.name = 'Mosh Hamedani';
  /* No se puede utizar course.author.save, esto no existe 
  course.save();
  */

  /* Forma 2: */
  const course = await Course.update({ _id: courseId }, {
    /* set para agregar y modificar campos del documento (siempre y cuando los campos
      ya existan en el schema). */
    $set: {
      'author.name': 'Mosh Hamedani Again',
    },
    /* unset elimina campos del documento*/
    $unset: {
      'author.bio': ''
    }
  });
}

async function addAuthor(courseId, author) {
  const course = await Course.findById(courseId);
  course.authors.push(author);
  course.save();
}

async function removeAuthor(courseId, authorId) {
  const course = await Course.findById(courseId);
  /* el objeto course.authors tiene un método id que regresa el subdocumento en base a su id. */
  const author = course.authors.id(authorId);
  author.remove();
  course.save();
}

/*
createCourse('Node Course', [
  new Author({ name: 'Mosh' }),
  new Author({ name: 'John' })
]);

 updateAuthor('5b6b04d7e8fd343c66e5604c');
*/



//addAuthor('5b6b089d42b4bd3ebaa72afa', new Author({ name: 'Dario' }));

removeAuthor('5b6b089d42b4bd3ebaa72afa', '5b6b094fdf3ec33f2202341d');