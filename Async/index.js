// console.log("Before");

/* En este caso lo asíncrono no quiere decir multithreaded o concurrente!!
Se usa un scheduler para ejecutar tareas que requieren de tiempo de completación.
El código asíncrono se utiliza para todas las llamadas a recursos fuera de la aplicación.
*/

/* en esta sección al momento de correr, el user da undefined porque no obtiene el valor sino hasta
dos segundos después. entopnces cuando el valor se lee, aun no se tiene. 


getUser(1, (result) => {
  getRepositories(result.githubUser, (repos) => {
    console.log("With repos " + console.log(repos));
  });
});
console.log("After");

*/


/* Para solventar el problema anterior, existen 3 approaches para sobrellevarlos: Callbacks, Promises y 
Async/await. El async es para dar a entender explícitamente que se trabajará con código asíncrono, pero dada
la naturaleza de node, los importantes son callbacks y promises */

/* Callbacks: funciones que reciben como parámetros otras funciones. Para evitar el callback hell se hace una 
separación de funciones. que se van llamando haciendo funciones que ya no son anónimas. Y se hace una cascada
de llamadas a funciones. Ej:

getUser(1, getRepositories);

function getRepositories(user) {
  getRepositories(user.gitHubUsername, getCommits);
}

function getCommits(repos) {
  getCommits(repo, displayCommits);
}

function displayCommits(commits) {
  console.log(commits);
}



-----------------------------------------------------------------------------


function getUser(id, callback) {
  setTimeout(() => {
    console.log("Reading something");
    callback({ id: id, githubUser: "Twireon" });
  }, 2000);
}

function getRepositories(username, callback) {
  setTimeout(() => {
    console.log("username " + username);
    callback(["repo1", "repo2", "repo3"]);
  }, 2000);
}



console.log('Before');
getUser(1, (user) => {
  getRepositories(user.gitHubUsername, (repos) => {
    getCommits(repos[0], (commits) => {
      console.log(commits);
    })
  })
});
*/

// Consumo de las promesas
/*
getUser(1).then(user => {
  getRepositories(user.gitHubUsername).then(repos => {
    getCommits(repos[0]).then(commits => console.log(commits))
    /* un simple catch para todos los posibles errores en todas las promesas 
    .catch(err => console.log('Error' + err.message))
  })
});
*/

/* Async and Await es lo mismo en c#, y permite escribir código asíncrono como síncrono.
cada vez que se llame a una función que devuelva una promesa se puede esperar el resultado con await
 Todo el código de await tiene que estar dentro de un bloque funcion con el decorador llamado async, 
que under the hood, convertirá el código en promesas. Sin embargo, en este approach no se tiene el catch,
y la manera de resolverlo es con un bloque trycatch  */

async function displayCommits() {
  try {
    const user = await getUser(1);
    const repos = await getRepositories(user);
    const commit = await getCommits(repos[0]);
    console.log(commit);
  } catch (err) {
    console.log('Error: ' + err);
  }
}

displayCommits();



console.log('After');

function getUser(id) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log('Reading a user from a database...');
      resolve({
        id: id,
        gitHubUsername: 'mosh'
      });
    }, 2000);
  });
}

function getRepositories(username) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log('Calling GitHub API...');
      resolve(['repo1', 'repo2', 'repo3']);
    }, 2000);
  });
}

function getCommits(repo) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log('Calling GitHub API...');
      resolve(['commit']);
    }, 2000);
  })
}