
/* Promises: Es un objeto que sostiene el resultado eventual de una operación asíncrona, es una herramienta poderosa
para manejar código asíncrono en javascript. La promesa arrojara o un valor o un error. 
Las promesas pueden tener 3 estados:

1. Pending (cuando se crea)
2. Operación asíncrona (cuando se ejecuta el código async)
3. Fulfilled or Error

*/

const p = new Promise((resolve, reject) => {
  /* Lanzar una operación asíncrona, eventualmente se tendrá un valor o un error y se devolverá al consumidor de esa promesa
  Se hace utilizando las funciones resolve y reject, ejemplo, aquí se le devuelve un valor con  la función resolve */
  setTimeout(() => {
    resolve(1);
    reject(new Error('message'));
  }, 2000);
  
});

/* el consumidor p que contiene la promesa tiene 2 métodos el catch que atrapa los posibles errores y el then que recibe una función
que tiene el resultado de éxito de la promesa, en este caso el valor de resolve */

p.then(result => console.log('Result: ' + result)).catch(err => console.log('Error: ' + err.message));

/* Como nota importante: Todas las funciones de callback tienen potencial de convertirse en promesas 
para cambiar los callbacks con promesas, se elimna el callback como parámetro en la firma de la función
y se regresa una promesa: 

---
Version callback: => 
---

function getRepositories(username, callback) {
  setTimeout(() => {
    console.log("username " + username);
    callback(["repo1", "repo2", "repo3"]);
  }, 2000);
}

---
Version Promise: => 
---

function getRepositories(username) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log("username " + username);
      resolve(["repo1", "repo2", "repo3"]);
    }, 2000);
  });
}

*/