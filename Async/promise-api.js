/* Análisis de la clase de Promise*/

/* cuando se rechazan las promesas siempre debe usarse un objeto error (new Error('error')) para los stacks*/
Promise.resolve({id: 1}).then(res => console.log(res));

/* Promesas paralelas: cuando se ejecutan dos o más promesas a la vez y es necesario que otro proceso se triggeree
después de que todas las promesas sean completadas. */

const p1 = new Promise((resolve, reject) => {
  setTimeout(() => {
    console.log('Asyn op 1...');
    resolve(1);
    // reject(new Error('because something failed'));
  }, 2100);
})

const p2 = new Promise((resolve) => {
  setTimeout(() => {
    console.log('Asyn op 2...');
    resolve(2);
  }, 2000);
})

/* método all de la clase promise, donde recibe un arreglo con las promesas que se quieren ejecutar en paralelo, este lanza
otra promesa que sera resuelta cuando todas las promesas dentro arreglo sean resueltas. Encuentro esto útil, como nota aparte. 
ambas promesas se empiezan a ejecutar casi al mismo tiempo. por la programación asíncrona. }

Para clarificar: aquí de nuevo no se está utilizando concurrencia. No existe más de un hilo de ejecución que realice estas tareas
sino que simplemente estan entrando a una cola de procesos  que se ejecutan casi a la vez por un solo hilo de ejecución que está 
lanzando las operaciones casi al mismo tiempo. La diferencia es que el hilo NO ESPERA a que la promesa resuelva e inmediatamente se
mueve a la siguiente línea de código. 

Cuando se encadenan promesas con then entonces sí, cada promesa previa espera el resultado de la anterior.

Otra cosa a notar es que la respuesta de la promesa all es un arreglo con los objetos que resuelve cada una de las promesas en el orden
original, por lo tanto la respuesta será: [1, 2] independientemente del que tarden en completarse o si uno subsecuente termina antes que 
uno anterior.

ahora, qué sucede cuando una de las promesas devuelve un error? 
En este caso sí una (cualquiera) promesa falla, se envía el error y todo el proceso en su conjunto fallará y en general será considerada
rejected.

*/

Promise.all([p1, p2]).then(result => console.log(result)).catch(err => console.log(('Error ' + err.message)));

/* En caso de que se quiera ver cual promesa se cumple al principio se utiliza el método race, el cual desplegará el valor del
primer resolve de la promesa que se haya completado primero y el resultado no es un arreglo, sino el valor de la promesa que se cumpla
primero. La sintaxis es igual al método de all*/